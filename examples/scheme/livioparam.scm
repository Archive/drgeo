;; Dr. Geo
;; (C) Copyright 2005
;; Author: Andrea Centomo 
;; mailto: acentomo@ofset.org
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public Licences as by published
;; by the Free Software Foundation; either version 2; or (at your option)
;; any later version
;;
;; This program is distributed in the hope that it will entertaining,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of 
;; MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;; Public License for more details.
;; This example implements a construction
;; from Livio's Italian book about the golden section

(new-figure "Tree")

(define pi (acos -1)) ;;pigreco

(lets Point "l" free  3 -5)
(lets Point "m" free  4 -5)
(lets Segment "ml" extremities m l)
(lets Point "mv" on-curve ml 0.618)
(lets Numeric "s" point-point 5 -5 mv m) ;; fattore di scala

(define (rami a b k)

  (let* (
	 (ab (Segment "" extremities a b))
	 (c (Point "" scale a b s))
	 (alfa (Numeric "" free 3 2 (/ (* 2 pi) 3)))
	 (d (Point "" rotation c b alfa))
	 (nalfa (Numeric "" free 3 1 (/ (* -2 pi) 3)))
	 (e (Point "" rotation c b nalfa))
	 (bd (Segment "" extremities b d))
	 (be (Segment "" extremities b e))
	 )

    ;; nascondiamo la costruzione

    (map (lambda (x) (send x masked))
	 (list alfa d nalfa e c))

    (if (> k 0)

    (begin
      (rami b e (- k 1 ))
      (rami b d (- k 1 ))))))

(lets Point "A" free  0 -5)
(lets Point "B" free  0 -2)

(send A masked)
(send B masked)

(rami A B 9)
