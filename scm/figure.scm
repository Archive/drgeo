(define (triangle p1 p2 p3)
  (Segment "" extremity p1 p2)
  (Segment "" extremity p2 p3)
  (Segment "" extremity p1 p3))

(define (hasard)
  (- 8 (* 16 (random:uniform))))

(new-figure "Ma figure")

(lets Point "A" free (hasard) 0)
(lets Point "B" free 5 0)
(lets Point "C" free (hasard) 5)
(lets Point "D" free -5 (hasard))

(triangle A B C)

(lets Point "I" middle-2pts A B)

(lets Line "l1" 2points A D)
(lets Ray "" 2points D C)
(lets Vector "" 2points D I)
(lets Circle "Cc" 2points I B)
(lets Numeric "a1" free 1 2 3.1415)
(lets Point "M" rotation A B a1)

(lets Segment "S" extremity A B)
(lets Point "mobile" on-curve S 1.5)

; Send message to change the attributes of our objects

(send Cc color red)
(send Cc thickness large)
(send A masked)
(send l1 thickness dashed)
(send B shape round)
(send B color bordeaux)
(send B size large)
