/*
 *  Dr Geo an interactive geometry software
 * (C) Copyright Hilaire Fernandes  1997-1999
 * hilaire.fernandes@iname.com 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>

#include "classbase.h"
#include "drgeo_tree.h"

GtkWidget *tree_window, *scrolled_win, *tree = NULL;

extern liste_elem liste_figure, selection;
extern GtkWidget *figure_drawing_area;

void
create_tree_window (void)
{
  tree_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect (GTK_OBJECT (tree_window), "delete_event",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
  gtk_container_set_border_width (GTK_CONTAINER (tree_window), 5);

  /* A generic scrolled window */
  scrolled_win = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_widget_set_usize (scrolled_win, 150, 200);
  gtk_container_add (GTK_CONTAINER (tree_window), scrolled_win);
  gtk_widget_show (scrolled_win);
}

void
create_tree (void)
{
  gint nb, i;
  if (tree)
    {
      gtk_container_remove (GTK_CONTAINER (scrolled_win), tree);
      gtk_object_unref (GTK_OBJECT (tree));
    }
  tree = gtk_tree_new ();
  gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled_win),
					 tree);
  /* Set the selection mode */
  gtk_tree_set_selection_mode (GTK_TREE (tree), GTK_SELECTION_MULTIPLE);
  /* Show it */
  gtk_widget_show (tree);

  nb = liste_figure.nb_elem;

  // put all the figure on the tree at level 0
  for (i = 1; i <= nb; i++)
    add_figure_on_tree (tree, (figure_c *) liste_figure.lire (i));

  gtk_widget_show (tree_window);
}

void
add_figure_on_tree (GtkWidget * tree, figure_c * fig)
{
  GtkWidget *item, *subtree;
  liste_elem parents;
  gint nb, i;

  item = gtk_tree_item_new_with_label (fig->nom_type);
  // Connect the needed signals
  gtk_signal_connect (GTK_OBJECT (item), "select",
		      GTK_SIGNAL_FUNC (tree_select_figure), (void *) fig);
  gtk_signal_connect (GTK_OBJECT (item), "deselect",
		      GTK_SIGNAL_FUNC (tree_deselect_figure), (void *) fig);

  // Add it to the tree
  gtk_tree_append (GTK_TREE (tree), item);
  gtk_widget_show (item);

  if (fig->parents (&parents) != NULL)
    {
      // we build a subtree for it's parents
      subtree = gtk_tree_new ();
      gtk_tree_set_view_mode (GTK_TREE (subtree), GTK_TREE_VIEW_ITEM);
      gtk_tree_item_set_subtree (GTK_TREE_ITEM (item), subtree);

      nb = parents.nb_elem;
      for (i = 1; i <= nb; i++)
	add_figure_on_tree (subtree, (figure_c *) parents.lire (i));
    }
}

// Event for the tree
void
tree_select_figure (GtkWidget * item, void *fig)
{
  // check if the figure is already selected
  if (selection.position (fig) == 0)
    selection.ajoute (fig);
}

void
tree_deselect_figure (GtkWidget * item, void *fig)
{
  // check if the figure is already selected
  selection.supprime (fig);
  ((figure_c *) fig)->dessine (figure_drawing_area->window, FALSE);
}
