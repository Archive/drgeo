#!/bin/sh


DEST="/tmp/drgeo"
CUR=`pwd`
rm -rf $DEST
mkdir $DEST
./configure --prefix=$DEST
make
make install

cd  $DEST/share/
tar cfz $CUR/drgeoLocale.tgz locale
cd $CUR

./configure --prefix=/usr
rm -rf $DEST
