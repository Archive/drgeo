#include "drgeo_exportLatexDialog.h"
#include "drgenius_mdi.h"
#include "geo/drgeo_gtkdrawable.h"
#include "geo/drgeo_point.h"

extern drgeniusMDI *mdi;

exportLatexdlg::exportLatexdlg (drgeoFigure *fig, liste_elem *figureList,
				int size_mode, int document_mode)
{
  int a, nb;
  geometricObject *obj;

  xml = glade_xml_new (DRGEO_GLADEDIR "/drgeo2.glade",
		       "exportLatexDialog", NULL);
  glade_xml_signal_autoconnect (xml);
  dlg = glade_xml_get_widget (xml, "exportLatexDialog");
  mdi->setTransientDialog (GTK_WINDOW (dlg));
  frame_size = glade_xml_get_widget (xml, "size");
  radio_aut = glade_xml_get_widget (xml, "automatic");
  gtk_signal_connect (GTK_OBJECT (radio_aut), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_automatic_toggled_static),
		      this);
  radio_fix = glade_xml_get_widget (xml, "fixed");
  gtk_signal_connect (GTK_OBJECT (radio_fix), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_fixed_toggled_static),
		      this);
  radio_scr = glade_xml_get_widget (xml, "screen");
  gtk_signal_connect (GTK_OBJECT (radio_scr), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_screen_toggled_static),
		      this);
  radio_sel = glade_xml_get_widget (xml, "selected");
  gtk_signal_connect (GTK_OBJECT (radio_sel), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_selected_toggled_static),
		      this);
  radio_comp = glade_xml_get_widget (xml, "complete");
  gtk_signal_connect (GTK_OBJECT (radio_comp), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_complete_toggled_static),
		      this);
  radio_pspict = glade_xml_get_widget (xml, "pspicture");
  gtk_signal_connect (GTK_OBJECT (radio_pspict), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_pspicture_toggled_static),
		      this);
  radio_graph = glade_xml_get_widget (xml, "graphics");
  gtk_signal_connect (GTK_OBJECT (radio_graph), "toggled",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_graphics_toggled_static),
		      this);
  xmin = (GtkSpinButton*) glade_xml_get_widget (xml, "xmin");
  ymin = (GtkSpinButton*) glade_xml_get_widget (xml, "ymin");
  xmax = (GtkSpinButton*) glade_xml_get_widget (xml, "xmax");
  ymax = (GtkSpinButton*) glade_xml_get_widget (xml, "ymax");
  edge = (GtkSpinButton*) glade_xml_get_widget (xml, "edge");
  gtk_signal_connect (GTK_OBJECT (edge), "value-changed",
		      GTK_SIGNAL_FUNC (exportLatexdlg::on_edge_value_changed_static),
		      this);

  drgeoGtkDrawable *area = (drgeoGtkDrawable *) fig->getDrawable ();

  drgeoPoint c = area->getAreaCenter ();
  drgeoPoint s = area->getAreaSize ();
  scr_br = area->getAreaCenter () - (area->getAreaSize () / 2);
  scr_tl = area->getAreaCenter () + (area->getAreaSize () / 2);
  
  if (area->printingArea ())
    {
      sel_br = area->getPrintingAreaBR ();
      sel_tl = area->getPrintingAreaTL ();
      if (sel_br.getX () > sel_tl.getX ())
	{
	  gdouble tmp = sel_br.getX ();
	  sel_br.setX (sel_tl.getX ());
	  sel_tl.setX (tmp);
	}
      if (sel_br.getY () > sel_tl.getY ())
	{
	  gdouble tmp = sel_br.getY ();
	  sel_br.setY (sel_tl.getY ());
	  sel_tl.setY (tmp);
	}
    }
  else
    {
      sel_br = scr_br;
      sel_tl = scr_tl;
    }

  nb = figureList->nb_elem;
  figureList->init_lire ();
  gboolean first = TRUE;

  for (a = 1; a <= nb; a++)
    {
      drgeoPoint pt;

      obj = (geometricObject *) figureList->lire (0);
      if (obj->getType () & (POINT + VECTOR))
	{
	  pt = ((point*) obj)->getCoordinate ();
	  if (first)
	    {
	      aut_br.setX (pt.getX ());
	      aut_tl.setX (pt.getX ());
	      aut_br.setY (pt.getY ());
	      aut_tl.setY (pt.getY ());
	      first = FALSE;
	    }
	  else
	    {
	      if (aut_br.getX () > pt.getX ()) aut_br.setX (pt.getX ());
	      if (aut_tl.getX () < pt.getX ()) aut_tl.setX (pt.getX ());
	      if (aut_br.getY () > pt.getY ()) aut_br.setY (pt.getY ());
	      if (aut_tl.getY () < pt.getY ()) aut_tl.setY (pt.getY ());
	    }
	}
    }

  this->size_mode = size_mode;
  this->document_mode = document_mode;
  edge_vect.setX (gtk_spin_button_get_value (edge));
  edge_vect.setY (gtk_spin_button_get_value (edge));

  switch (this->size_mode)
    {
    case LATEXDLG_AUTOMATIC:
      fix_br = aut_br - edge_vect;
      fix_tl = aut_tl + edge_vect;
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_aut), TRUE);
      break;
    case LATEXDLG_FIXED:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_fix), TRUE);
      break;
    case LATEXDLG_SCREEN:
      fix_br = scr_br;
      fix_tl = scr_tl;
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_scr), TRUE);
      break;
    case LATEXDLG_SELECTED:
      fix_br = sel_br;
      fix_br = sel_br;
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_sel), TRUE);
      break;
    }

  if (this->size_mode != LATEXDLG_AUTOMATIC)
    gtk_widget_set_sensitive (GTK_WIDGET (edge), FALSE);

  if (this->size_mode != LATEXDLG_FIXED)
    {
      gtk_widget_set_sensitive (GTK_WIDGET (xmin), FALSE);
      gtk_widget_set_sensitive (GTK_WIDGET (ymin), FALSE);
      gtk_widget_set_sensitive (GTK_WIDGET (xmax), FALSE);
      gtk_widget_set_sensitive (GTK_WIDGET (ymax), FALSE);
    }

  switch (this->document_mode)
    {
    case LATEXDLG_COMPLETE:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_comp), TRUE);
      break;
    case LATEXDLG_PSPICTURE:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_pspict), TRUE);
      break;
    case LATEXDLG_GRAPHICS:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (radio_graph), TRUE);
      break;
    }

  if (this->document_mode == LATEXDLG_GRAPHICS)
    gtk_widget_set_sensitive (GTK_WIDGET (frame_size), FALSE);

  update_fixed ();
}

void
exportLatexdlg::update_fixed ()
{
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (xmin), fix_br.getX ());
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ymin), fix_br.getY ());
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (xmax), fix_tl.getX ());
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (ymax), fix_tl.getY ());
}

gboolean
exportLatexdlg::run ()
{
  gboolean code = gtk_dialog_run (GTK_DIALOG (dlg));

  gtk_widget_destroy (dlg);

  switch (code)
    {
    case GTK_RESPONSE_OK:
      return TRUE;
    case GTK_RESPONSE_REJECT:
      return FALSE;
    }
}

drgeoPoint &
exportLatexdlg::getBR ()
{
  switch (size_mode)
    {
    case LATEXDLG_AUTOMATIC:
      return (aut_br - edge_vect);
    case LATEXDLG_FIXED:
      return fix_br;
    case LATEXDLG_SCREEN:
      return scr_br;
    case LATEXDLG_SELECTED:
      return sel_br;
    }
}

drgeoPoint &
exportLatexdlg::getTL ()
{
  switch (size_mode)
    {
    case LATEXDLG_AUTOMATIC:
      return (aut_tl + edge_vect);
    case LATEXDLG_FIXED:
      return fix_tl;
    case LATEXDLG_SCREEN:
      return scr_tl;
    case LATEXDLG_SELECTED:
      return sel_tl;
    }
}

int
exportLatexdlg::get_document_type ()
{
  return document_mode;
}

void
exportLatexdlg::on_automatic_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_automatic_toggled ();
}

void
exportLatexdlg::on_automatic_toggled ()
{
  if (size_mode == LATEXDLG_AUTOMATIC) // Automatic uncheck
    {
      gtk_widget_set_sensitive (GTK_WIDGET (edge), FALSE);
    }
  else // Automatic check
    {
      gtk_widget_set_sensitive (GTK_WIDGET (edge), TRUE);
      fix_br = aut_br - edge_vect;
      fix_tl = aut_tl + edge_vect;
      update_fixed ();
      size_mode = LATEXDLG_AUTOMATIC;
    }
}

void
exportLatexdlg::on_fixed_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_fixed_toggled ();
}

void
exportLatexdlg::on_fixed_toggled ()
{
  if (size_mode == LATEXDLG_FIXED) // Fixed uncheck
    {
      gtk_widget_set_sensitive (GTK_WIDGET (xmin), FALSE);
      gtk_widget_set_sensitive (GTK_WIDGET (ymin), FALSE);
      gtk_widget_set_sensitive (GTK_WIDGET (xmax), FALSE);
      gtk_widget_set_sensitive (GTK_WIDGET (ymax), FALSE);
    }
  else // Fixed check
    {
      gtk_widget_set_sensitive (GTK_WIDGET (xmin), TRUE);
      gtk_widget_set_sensitive (GTK_WIDGET (ymin), TRUE);
      gtk_widget_set_sensitive (GTK_WIDGET (xmax), TRUE);
      gtk_widget_set_sensitive (GTK_WIDGET (ymax), TRUE);
      size_mode = LATEXDLG_FIXED;
    }
}

void
exportLatexdlg::on_screen_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_screen_toggled ();
}

void
exportLatexdlg::on_screen_toggled ()
{
  if (!(size_mode == LATEXDLG_SCREEN)) // Screen check
    {
      fix_br = scr_br;
      fix_tl = scr_tl;
      update_fixed ();
      size_mode = LATEXDLG_SCREEN;
    }
}

void
exportLatexdlg::on_selected_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_selected_toggled ();
}

void
exportLatexdlg::on_selected_toggled ()
{
  if (!(size_mode == LATEXDLG_SELECTED)) // Selected check
    {
      fix_br = sel_br;
      fix_tl = sel_tl;
      update_fixed ();
      size_mode = LATEXDLG_SELECTED;
    }
}

void
exportLatexdlg::on_complete_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_complete_toggled ();
}

void
exportLatexdlg::on_complete_toggled ()
{
  if (!(document_mode == LATEXDLG_COMPLETE)) // Complete check
    {
      document_mode = LATEXDLG_COMPLETE;
    }
}

void
exportLatexdlg::on_pspicture_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_pspicture_toggled ();
}

void
exportLatexdlg::on_pspicture_toggled ()
{
  if (!(document_mode == LATEXDLG_PSPICTURE)) // Pspicture check
    {
      document_mode = LATEXDLG_PSPICTURE;
    }
}

void
exportLatexdlg::on_graphics_toggled_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_graphics_toggled ();
}

void
exportLatexdlg::on_graphics_toggled ()
{
  if (!(document_mode == LATEXDLG_GRAPHICS)) // Graphics check
    {
      gtk_widget_set_sensitive (GTK_WIDGET (frame_size), FALSE);
      document_mode = LATEXDLG_GRAPHICS;
    }
  else // Graphics uncheck
    {
      gtk_widget_set_sensitive (GTK_WIDGET (frame_size), TRUE);
    }
}

void
exportLatexdlg::on_edge_value_changed_static
  (GtkWidget *widget, gpointer data)
{
  ((exportLatexdlg*) data)->on_edge_value_changed ();
}

void
exportLatexdlg::on_edge_value_changed ()
{
  edge_vect.setX (gtk_spin_button_get_value (edge));
  edge_vect.setY (gtk_spin_button_get_value (edge));
  fix_br = aut_br - edge_vect;
  fix_tl = aut_tl + edge_vect;

  update_fixed ();
}
