/* Dr Genius an interactive geometry software
 * (C) Copyright Hilaire Fernandes  2003
 * hilaire@ofset.org 
 * 
 *
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef DRGEO_EXPORTLATEXDIALOG_H
#define DRGEO_EXPORTLATEXDIALOG_H

#include <glade/glade.h>

#include "drgenius_mdi.h"
#include "geo/drgeo_figure.h"

#define LATEXDLG_AUTOMATIC 0
#define LATEXDLG_FIXED 1
#define LATEXDLG_SCREEN 2
#define LATEXDLG_SELECTED 3
#define LATEXDLG_COMPLETE 1
#define LATEXDLG_PSPICTURE 2
#define LATEXDLG_GRAPHICS 4

class exportLatexdlg
{
 public:
  exportLatexdlg (drgeoFigure *fig, liste_elem *figureList,
		  int size_mode, int document_mode);
  gboolean run ();
  void update_fixed ();
  drgeoPoint & getBR ();
  drgeoPoint & getTL ();
  int get_document_type ();
  static void on_automatic_toggled_static (GtkWidget *widget, gpointer data);
  void on_automatic_toggled ();
  static void on_fixed_toggled_static (GtkWidget *widget, gpointer data);
  void on_fixed_toggled ();
  static void on_screen_toggled_static (GtkWidget *widget, gpointer data);
  void on_screen_toggled ();
  static void on_selected_toggled_static (GtkWidget *widget, gpointer data);
  void on_selected_toggled ();
  static void on_edge_value_changed_static (GtkWidget *widget, gpointer data);
  void on_edge_value_changed ();
  static void on_complete_toggled_static (GtkWidget *widget, gpointer data);
  void on_complete_toggled ();
  static void on_pspicture_toggled_static (GtkWidget *widget, gpointer data);
  void on_pspicture_toggled ();
  static void on_graphics_toggled_static (GtkWidget *widget, gpointer data);
  void on_graphics_toggled ();
 private:
  GladeXML *xml;
  GtkWidget *dlg, *frame_size;
  GtkWidget *radio_aut, *radio_fix, *radio_scr, *radio_sel;
  GtkWidget *radio_comp, *radio_pspict, *radio_graph;
  GtkSpinButton *xmin, *ymin, *xmax, *ymax, *edge;
  drgeoPoint sel_br, sel_tl, scr_br, scr_tl, aut_br, aut_tl, fix_br, fix_tl,
    edge_vect;
  int size_mode, document_mode;
};

#endif /* DRGEO_EXPORTLATEXDIALOG_H */
