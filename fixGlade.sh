#!/bin/sh

DEST="/tmp/drgeo"
CUR=`pwd`

gladeFile="drgenius2.glade drgeo2.glade drgeoMDI.glade"

rm -rf $DEST
mkdir $DEST $DEST/glade

for f in $gladeFile
do
   sed glade/$f -e 's/"icon">\(.*\)</"icon">glade\/\1</g' > $DEST/glade/$f
done

cd $DEST
tar cfz $CUR/drgeoGladeWin32.tgz glade
cd $CUR

rm -rf $DEST